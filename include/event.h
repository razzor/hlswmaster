#ifndef _EVENT_H_
#define _EVENT_H_

#include <sys/time.h>

#define FD_READ		0x01
#define FD_WRITE	0x02
#define FD_EXCEPT	0x04

#define event_add_readfd(fd, callback, privdata) \
	event_add_fd(fd, FD_READ, callback, privdata)

#define event_add_writefd(fd, callback, privdata) \
	event_add_fd(fd, FD_WRITE, callback, privdata)

#define event_add_exceptfd(fd, callback, privdata) \
	event_add_fd(fd, FD_EXCEPT, callback, privdata)

int event_add_fd(int fd, int type, int (*callback)(int fd, void *privdata), void *privdata);
int event_add_timeout(struct timeval *timeout, int (*callback)(void *privdata), void *privdata);

int event_remove_fd(int fd);

void event_loop(void);

#endif /* _EVENT_H_ */
