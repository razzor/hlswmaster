#ifndef _PLUGIN_HELPER_H
#define _PLUGIN_HELPER_H

struct scan_ports {
	unsigned short portlo;
	unsigned short porthi;
	unsigned short gameid;
};

int server_add(uint16_t gameid, uint32_t ip, uint16_t port1, uint16_t port2);
int server_add_pkt(unsigned int gameid, struct net_pkt *pkt);
int pkt_send_portarr(struct in_addr *dstip, struct scan_ports *portarr, char *buf, unsigned int size);
int pkt_check_portarr(struct net_pkt *pkt, struct scan_ports *portarr);

#endif /* _PLUGIN_HELPER_H */
