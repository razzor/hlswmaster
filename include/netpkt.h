#ifndef _NETPKT_H
#define _NETPKT_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include "list.h"

struct net_pkt {
	struct list_head list;
	struct sockaddr_in addr;
	unsigned int size;
	unsigned char buf[0];
};

int pkt_memcmp(struct net_pkt *pkt, unsigned int offset, char *search, unsigned int size);
int pkt_memmem(struct net_pkt *pkt, unsigned int offset, char *search, unsigned int size);
struct net_pkt * pkt_merge(struct net_pkt *pkt1, struct net_pkt *pkt2);

char * pkt_ntoa(struct net_pkt *pkt);
unsigned short pkt_getport(struct net_pkt *pkt);
int pkt_sameaddr(struct net_pkt *pkt1, struct net_pkt *pkt2);
int pkt_parse_int(struct net_pkt *pkt, unsigned int offset, int *val);
int pkt_parse_ip(struct net_pkt *pkt, int offset, struct in_addr *ip);
char * pkt_print(struct net_pkt *pkt);

#endif /* _NETPKT_H */
