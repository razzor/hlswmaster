#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <netinet/in.h>

int config_parse(const char *config);

char * config_get_string(const char *section_str, const char *option, char *def);

int config_get_int(const char *section, const char *option, int def);

int config_get_strings(const char *section_str, const char *option,
			int (*callback)(const char *value, void *privdata),
			void *privdata);

int parse_saddr(const char *addr, struct sockaddr_in *sa);

#endif /* _CONFIG_H_ */
