#ifndef _GAMELIST_H_
#define _GAMELIST_H_

#include <inttypes.h>

#include "list.h"

struct game_entry {
	struct list_head list;
	unsigned long modtime;

	/* begin HLSW_ENTRY */
	uint16_t gameid;
	uint32_t ip;
	uint16_t port1;
	uint16_t port2;
	/* end HLSW_ENTRY */

} __attribute__ ((packed));

int gamelist_init();
int gamelist_add(struct game_entry *entry);
int gamelist_gc_and_dump(int (*callback)(struct game_entry *entry), int timeout);

#endif /* _GAMELIST_H_ */
