#ifndef _PLUGIN_H
#define _PLUGIN_H

#include "netpkt.h"
#include "configfile.h"
#include "list.h"

// paket nicht akzeptiert, free() muss noch aufgerufen werden
#define PARSE_REJECT 0

// paket akzeptiert, free() muss noch aufgerufen werden
#define PARSE_ACCEPT 1

// paket akzeptiert, free() wurde schon, oder darf noch aufgerufen werden
#define PARSE_ACCEPT_FREED 2

struct hlswmaster_plugin {
	struct list_head list;
	char name[32];

	int (*init)(void);
	int (*fini)(void);
	int (*scan)(void);
	int (*parse)(struct net_pkt *pkt);
};

int plugin_init();
int plugins_scan();
int plugins_parse(struct net_pkt *pkt);

#endif /* _PLUGIN_H */
