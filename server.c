/***************************************************************************
 *   Copyright (C) 11/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include "configfile.h"
#include "list.h"
#include "event.h"
#include "scanner.h"
#include "logging.h"
#include "netpkt.h"
#include "gamelist.h"

#define HLSW_HEADER "\xFF\xFF\xFF\xFFHLSWLANSEARCH"
#define HLSW_HEADER_LEN 0x11
#define HLSW_ENTRY_LEN 10
#define MAX_PKT_LEN (HLSW_HEADER_LEN + HLSW_ENTRY_LEN * 140)

static LIST_HEAD(master_pkt_list);

static int serverlist_add_game(struct game_entry *entry)
{
	int found = 0;

	struct net_pkt *pkt;
	list_for_each_entry(pkt, &master_pkt_list, list) {
		if (pkt->size <= (MAX_PKT_LEN - HLSW_ENTRY_LEN)) {
			found = 1;
			break;
		}
	}

	if (!found) {
		pkt = malloc(sizeof(struct net_pkt) + MAX_PKT_LEN);
		if (pkt == NULL) {
			log_print(LOG_WARN, "serverlist_add_game(): out of memory");
			return -1;
		}

		memcpy(pkt->buf, HLSW_HEADER, HLSW_HEADER_LEN);
		pkt->size = HLSW_HEADER_LEN;
		list_add(&pkt->list, &master_pkt_list);
	}

	memcpy((void *)&pkt->buf + pkt->size, &entry->gameid, HLSW_ENTRY_LEN);
	pkt->size += HLSW_ENTRY_LEN;
	return 0;
}

static int serverlist_refresh(void *privdata)
{
	int timeout = (int)privdata;

	struct net_pkt *pkt, *tmp;
	list_for_each_entry_safe(pkt, tmp, &master_pkt_list, list) {
		list_del(&pkt->list);
		free(pkt);
	}

	gamelist_gc_and_dump(serverlist_add_game, timeout);
	return 0;
}

static int server_handler(int fd, void *privdata)
{
	struct sockaddr_in client;
	unsigned char buf[32];

	unsigned int i = sizeof(client);
	int ret = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&client, &i);
	if (ret <= 0) {
		log_print(LOG_WARN, "server_handler(): recvfrom()");
		return 0;
	}

	if (memcmp(buf, HLSW_HEADER, HLSW_HEADER_LEN))
		return 0;

	struct net_pkt *pkt;
	list_for_each_entry(pkt, &master_pkt_list, list)
		sendto(fd, pkt->buf, pkt->size, 0, (struct sockaddr *)&client, sizeof(client));

	return 0;
}

int server_init()
{
	int sock = socket(PF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		log_print(LOG_ERROR, "server_init(): socket()");
		return -1;
	}

	struct sockaddr_in src;
	char *addr = config_get_string("global", "master_src", "0.0.0.0:7140");
	if (parse_saddr(addr, &src) != 0) {
		log_print(LOG_ERROR, "server_init(): invalid master_src '%s'", addr);
		return -1;
	}

	if (bind(sock, (struct sockaddr *)&src, sizeof(src)) < 0) {
		log_print(LOG_ERROR, "server_init(): bind()");
		return -1;
	}

	event_add_readfd(sock, server_handler, NULL);

	struct timeval tv;
	tv.tv_sec = config_get_int("global", "serverlist_refresh", 5);
	tv.tv_usec = 0;

	int timeout = config_get_int("global", "serverlist_timeout", 180);
	event_add_timeout(&tv, serverlist_refresh, (void *)timeout);
	return 0;
}
