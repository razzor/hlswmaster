/***************************************************************************
 *   Copyright (C) 03/2005 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <sys/stat.h>
#include <pwd.h>

#include "configfile.h"
#include "event.h"
#include "logging.h"

#include "gamelist.h"
#include "scanner.h"
#include "server.h"
#include "plugin.h"

#define DEFAULT_CONFIG "hlswmaster.conf"
#define DEFAULT_LOGFILE "hlswmaster.log"

static struct option opts[] = {
	{"config",	1, 0, 'c'},
	{"user",	1, 0, 'u'},
	{"debug",	0, 0, 'd'},
	{"help",	0, 0, 'h'},
	{0, 0, 0, 0}
};

int main(int argc, char *argv[])
{
	int arg = 0, code = 0, debug = 0;
	char *config = DEFAULT_CONFIG, *user = NULL, *logfile;

	while (code != -1) {
		code = getopt_long(argc, argv, "c:u:dh", opts, &arg);

		switch (code) {
		case 'c':	/* config */
				config = optarg;
				break;

		case 'u':	/* user */
				user = optarg;
				break;

		case 'd':	/* debug */
				debug = 1;
				break;

		case 'h':	/* help */
				printf("Usage: hlsw-master [options]\n"
					"Options: \n"
					"  --config       -c  configfile  use this configfile\n"
					"  --user         -u  username    change uid to username\n"
					"  --debug        -d              do not fork and log to stderr\n"
					"  --help         -h              this help\n"
					"\n");
				exit(0);
				break;

		case '?':	/* error */
				exit(-1);
				break;

		default:	/* unknown / all options parsed */
				break;
		}
	}

	/* userwechsel */
	if (user) {
		struct  passwd *pwl;
		if (!(pwl = getpwnam(user))) {
			log_print(LOG_ERROR, "unknown user: %s", user);
			exit(-1);
		}

		if (setgid(pwl->pw_gid) || setuid(pwl->pw_uid)) {
			log_print(LOG_ERROR, "setgid/setuid");
			exit(-1);
		}
	}

	/* parse config file */
	if (config_parse(config) == -1)
		exit(-1);

	/* check logfile */
	logfile = config_get_string("global", "logfile", DEFAULT_LOGFILE);
	if (logfile && !debug) {
		/* start logging */
		if (!log_init(logfile))
			exit(-1);

		/* zum daemon mutieren */
		daemon(-1, 0);
	}

	log_print(LOG_INFO, "hlswmaster started (user: %s, pid: %d)", getpwuid(getuid())->pw_name, getpid());

	/* init plugins */
	if (plugin_init() == -1)
		exit(-1);

	/* init gamelist */
	if (gamelist_init() == -1)
		exit(-1);

	/* init server */
	if (server_init() == -1)
		exit(-1);

	/* init scanner */
	if (scanner_init() == -1)
		exit(-1);

	event_loop();

	return 0;
}
