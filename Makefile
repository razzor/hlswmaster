VERSION := v2.00

CFLAGS := -O2 -pipe -Wall -Iinclude -DVERSION='"$(VERSION)"'
LDFLAGS := -ldl -rdynamic

OBJS := configfile.o event.o gamelist.o logging.o netpkt.o plugin.o \
	plugin_helper.o scanner.o server.o

all: hlswmaster masterquery
	make -C plugins all

hlswmaster: $(OBJS) hlswmaster.o
	$(CC) $(CFLAGS) $^ $(LDFLAGS) -o $@

masterquery: masterquery.o
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.d: %.c
	$(CC) $(CFLAGS) -MM -c $< -o $@

clean:
	rm -f hlswmaster masterquery *.d *.o *.log
	make -C plugins clean

DEPS := $(wildcard *.c)
-include $(DEPS:.c=.d)
