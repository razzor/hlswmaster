/***************************************************************************
 *   Copyright (C) 03/2005 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "gamelist.h"
#include "logging.h"
#include "netpkt.h"
#include "plugin_helper.h"
#include "scanner.h"

int server_add(uint16_t gameid, uint32_t ip, uint16_t port1, uint16_t port2)
{
	struct game_entry *entry = malloc(sizeof(struct game_entry));
	if (entry == NULL) {
		log_print(LOG_WARN, "server_add(): out of memory");
		return -1;
	}

	entry->gameid = gameid;
	entry->ip = ip;
	entry->port1 = port1;
	entry->port2 = port2;

	return gamelist_add(entry);
}

int server_add_pkt(unsigned int gameid, struct net_pkt *pkt)
{
	return server_add(gameid, pkt->addr.sin_addr.s_addr, ntohs(pkt->addr.sin_port), 0);
}

int pkt_send_portarr(struct in_addr *dstip, struct scan_ports *portarr, char *buf, unsigned int size)
{
	unsigned short port;
	int ret = 0;

	while (portarr && portarr->portlo) {
		for (port = portarr->portlo; port <= portarr->porthi; port++)
			if (pkt_send(dstip, port, buf, size) < 0)
				ret = -1;

		portarr++;
	}
	return ret;
}

int pkt_check_portarr(struct net_pkt *pkt, struct scan_ports *portarr)
{
	unsigned short port;
	while (portarr && portarr->portlo) {
		for (port = portarr->portlo; port <= portarr->porthi; port++)
			if (port == ntohs(pkt->addr.sin_port))
				return portarr->gameid;

		portarr++;
	}
	return 0;
}
