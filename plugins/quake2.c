/***************************************************************************
 *   Copyright (C) 03/2005 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "netpkt.h"
#include "plugin.h"
#include "plugin_helper.h"
#include "scanner.h"

static char scanmsg[] = "\xff\xff\xff\xffinfo 34"; /* q2(3) */
static char replymsg[] = "\xff\xff\xff\xffinfo";

static int scan(void)
{
	pkt_send(NULL, 27910, scanmsg, strlen(scanmsg));
	return 1;
}

static int parse(struct net_pkt *pkt)
{
	if (pkt_getport(pkt) != 27910)
		return PARSE_REJECT;

	if (pkt_memcmp(pkt, 0, replymsg, strlen(replymsg)))
		return PARSE_REJECT;

	server_add_pkt(3, pkt);
	return PARSE_ACCEPT;
}

struct hlswmaster_plugin plugin = {
	.name		= "quake2",
	.scan		= &scan,
	.parse		= &parse,
};
