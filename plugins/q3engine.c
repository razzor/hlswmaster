/***************************************************************************
 *   Copyright (C) 03/2005 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "netpkt.h"
#include "plugin.h"
#include "plugin_helper.h"

static struct scan_ports port_arr[] = {
	{ 27960, 27969,  6 }, /* q3(6), ef(7), et25), rtcw(8) */
	{ 28070, 28070, 12 }, /* jk2(12) */
	{ 28960, 28963, 31 }, /* cod(31), cod:uo(42), cod2(48) */
	{ 29070, 29070, 27 }, /* jk3(27) */
	{ 0,0,0 }
};

static char scanmsg[] = "\xff\xff\xff\xffgetStatus";
static char replymsg[] = "\xff\xff\xff\xffstatusResponse";
static char search_version[] = "\\version\\";
static char search_gamename[] = "\\gamename\\";
static char reply_q3[] = "Q3 ";
static char reply_ef[] = "ST:V HM ";
static char reply_rtcw[] = "Wolf ";
static char reply_et[] = "ET";
static char reply_jk2[] = "JK2MP";
static char reply_jk3[] = "JAmp";
static char reply_cod[] = "Call of Duty\\";
static char reply_coduo[] = "CoD:United Offensive\\";
static char reply_cod2[] = "Call of Duty 2\\";

static int scan(void)
{
	pkt_send_portarr(NULL, port_arr, scanmsg, strlen(scanmsg));
	return 1;
}

static int parse(struct net_pkt *pkt)
{
	int gameid = 0, pos1, pos2;

	if (!pkt_check_portarr(pkt, port_arr))
		return PARSE_REJECT;

	if (pkt_memcmp(pkt, 0, replymsg, strlen(replymsg)))
		return PARSE_REJECT;

	pos1 = pkt_memmem(pkt, 0, search_version, strlen(search_version));
	pos2 = pkt_memmem(pkt, 0, search_gamename, strlen(search_gamename));

	if (pos1 != -1) {
		pos1 += strlen(search_version);
		if (!pkt_memcmp(pkt, pos1, reply_q3, strlen(reply_q3)))
			gameid = 6;

		else if (!pkt_memcmp(pkt, pos1, reply_ef, strlen(reply_ef)))
			gameid = 7;

		else if (!pkt_memcmp(pkt, pos1, reply_rtcw, strlen(reply_rtcw)))
			gameid = 8;

		else if (!pkt_memcmp(pkt, pos1, reply_et, strlen(reply_et)))
			gameid = 25;

		else if (!pkt_memcmp(pkt, pos1, reply_jk2, strlen(reply_jk2)))
			gameid = 12;

		else if (!pkt_memcmp(pkt, pos1, reply_jk3, strlen(reply_jk3)))
			gameid = 27;
	}

	if (gameid == 0 && pos2 != -1) {
		pos2 += strlen(search_gamename);
		if (!pkt_memcmp(pkt, pos2, reply_cod, strlen(reply_cod)))
			gameid = 31;

		else if (!pkt_memcmp(pkt, pos2, reply_coduo, strlen(reply_coduo)))
			gameid = 42;

		else if (!pkt_memcmp(pkt, pos2, reply_cod2, strlen(reply_cod2)))
			gameid = 48;
	}

	if (gameid == 0)
		return PARSE_REJECT;

	server_add_pkt(gameid, pkt);
	return PARSE_ACCEPT;
}

struct hlswmaster_plugin plugin = {
	.name		= "q3engine",
	.scan		= &scan,
	.parse		= &parse,
};
