/***************************************************************************
 *   Copyright (C) 03/2005 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <inttypes.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "netpkt.h"
#include "plugin.h"
#include "plugin_helper.h"
#include "logging.h"
#include "scanner.h"

struct game_entry {
	uint16_t gameid;
	uint32_t ip;
	uint16_t port1;
	uint16_t port2;
} __attribute__ ((packed));

struct master_entry {
	struct list_head list;
	struct sockaddr_in addr;
};

static LIST_HEAD(master_list);

static char scanmsg[] = "\xff\xff\xff\xffHLSWLANSEARCH";

static int scan(void)
{
	struct master_entry *entry;
	list_for_each_entry(entry, &master_list, list)
		pkt_send(&entry->addr.sin_addr, ntohs(entry->addr.sin_port), scanmsg, sizeof(scanmsg));

	return 1;
}

static int parse(struct net_pkt *pkt)
{
	// TODO: check against master_list
	if (pkt_getport(pkt) != 7140)
		return PARSE_REJECT;

	struct game_entry *game = (void *)pkt->buf + sizeof(scanmsg);

	while ((void *)game < ((void *)pkt->buf + pkt->size)) {
		server_add(game->gameid, game->ip, game->port1, game->port2);
		game++;
	}

	return PARSE_ACCEPT;
}

static int init_callback(const char *value, void *privdata)
{
	struct master_entry *entry = malloc(sizeof(struct master_entry));
	if (entry == NULL) {
		log_print(LOG_ERROR, "hlswproxy_init(): out of memory");
		return -1;
	}

	if (parse_saddr(value, &entry->addr) != 0) {
		log_print(LOG_WARN, "  invalid dst: %s", value);
		free(entry);
		return -1;
	}

	log_print(LOG_INFO, "  adding remote master %s:%d",
		inet_ntoa(entry->addr.sin_addr), ntohs(entry->addr.sin_port));

	list_add(&entry->list, &master_list);
	return 0;
}

static int init(void)
{
	if (config_get_strings("hlswproxy", "scan", init_callback, NULL) <= 0)
		return -1;

	return 0;
}

struct hlswmaster_plugin plugin = {
	.name		= "hlswproxy",
	.scan		= &scan,
	.parse		= &parse,
	.init		= &init,
};
