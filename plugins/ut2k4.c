/***************************************************************************
 *   Copyright (C) 09/2005 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "netpkt.h"
#include "scanner.h"
#include "plugin.h"
#include "plugin_helper.h"

static char scanmsg1[] = "\x80\x00\x00\x00\x00";

static int scan(void)
{
	pkt_send(NULL, 10777, scanmsg1, 5);
	return 1;
}

static int parse(struct net_pkt *pkt)
{
	unsigned short *port;

	if (pkt_memcmp(pkt, 0, scanmsg1, 5))
		return PARSE_REJECT;

	if (pkt->size < 12)
		return PARSE_REJECT;

	port = (unsigned short *)&(pkt->buf[10]);
	server_add(33, pkt->addr.sin_addr.s_addr, *port, ntohs(pkt->addr.sin_port));
	return PARSE_ACCEPT;
}

struct hlswmaster_plugin plugin = {
	.name		= "ut2k4",
	.scan		= &scan,
	.parse		= &parse,
};
