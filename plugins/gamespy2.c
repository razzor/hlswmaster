/***************************************************************************
 *   Copyright (C) 03/2005 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "netpkt.h"
#include "plugin.h"
#include "plugin_helper.h"

static struct scan_ports port_arr[] = {
	{  2302,  2302, 30 }, /* halo(30) */
	{  3455,  3455, 37 }, /* painkiller(34) */
	{ 10481, 10482, 44 }, /* swat4 (44) */
	{ 29900, 29910, 45 }, /* battlefield2 (45) */
	{     0,     0,  0 }
};

static char scanmsg[] = { 0xFE, 0xFD, 0x00, 0xDE, 0xAD, 0xBE, 0xEF, 0xFF, 0x00, 0x00 };
static char replymsg[] = { 0x00, 0xDE, 0xAD, 0xBE, 0xEF };

static char search_hostport[] = "hostport";
static char search_gamename[] = "gamename";

static char reply_bf2[] = "battlefield2";

static int scan(void)
{
	pkt_send_portarr(NULL, port_arr, scanmsg, sizeof(scanmsg));
	return 1;
}

static int parse(struct net_pkt *pkt)
{
	int gameid, pos1, pos3, port;

	if (!(gameid = pkt_check_portarr(pkt, port_arr)))
		return PARSE_REJECT;

	if (pkt_memcmp(pkt, 0, replymsg, sizeof(replymsg)))
		return PARSE_REJECT;

	pos1 = pkt_memmem(pkt, 0, search_gamename, strlen(search_gamename));
	pos1 += strlen(search_gamename) +1;

	/* hostport angabe suchen */
	pos3 = pkt_memmem(pkt, 0, search_hostport, strlen(search_hostport));
	if (pos3 != -1)
		pkt_parse_int(pkt, pos3 + strlen(search_hostport) +1, &port);

	switch (gameid) {
	case 30:/* halo */
	case 37:/* painkiller */
		break;

	case 44:/* swat4 */
		if (pos3 != -1)
			gameid = 44;
		else
			return PARSE_REJECT;
		break;

	case 45:/* battlefield 2 */
		// todo: pos3 check noetig?
		if (!pkt_memcmp(pkt, pos1, reply_bf2, strlen(reply_bf2)) && pos3 != -1)
			gameid = 45;
		else
			return PARSE_REJECT;
		break;

	default:
		return PARSE_REJECT;
		break;
	}

	/*
	 * wenn ein hostport angegeben wurde, und das nicht der src port ist
	 * beide ports in die serverliste uebernehmen
	 */
	if ((pos3 != -1) && (port != ntohs(pkt->addr.sin_port))) {
		server_add(gameid, pkt->addr.sin_addr.s_addr, port, ntohs(pkt->addr.sin_port));
	} else {
		server_add_pkt(gameid, pkt);
	}

	return PARSE_ACCEPT;
}

struct hlswmaster_plugin plugin = {
	.name		= "gamespy2",
	.scan		= &scan,
	.parse		= &parse,
};
