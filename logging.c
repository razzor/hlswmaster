/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>

#include "logging.h"

#define BUFSIZE 8192

static FILE *log_fd = NULL;
static char *buffer = NULL;

void log_print(int prio, const char *fmt, ...)
{
	va_list az;
	int len;

	if (buffer == NULL) {
		buffer = malloc(BUFSIZE);
		if (buffer == NULL) {
			fprintf(stderr, "log_print: out of memory\nBailing out!\n");
			exit(-1);
		}
	}

	va_start(az, fmt);
	len = vsnprintf(buffer, BUFSIZE, fmt, az);
	va_end(az);

	if (len < 0 || len >= BUFSIZE) {
		log_print(LOG_ERROR, "log_print: arguments too long");
		errno = 0;
		return;
	}

	if (errno) {
		strncpy(buffer + len, ": ", BUFSIZE - len);
		len += 2;
		strncpy(buffer + len, strerror(errno), BUFSIZE - len);
	}

	if (log_fd) {
		char    tbuf[64];
		time_t  tzgr;

		time(&tzgr);
		strftime(tbuf, sizeof(tbuf), "%b %d %H:%M:%S :", localtime(&tzgr));

		fprintf(log_fd, "%s %s\n", tbuf, buffer);
		fflush(log_fd);

	} else {
		fprintf(stderr, "%s\n", buffer);
	}

	errno = 0;
}

static void log_close(void)
{
	if (buffer)
		free(buffer);

	fclose(log_fd);
}

int log_init(char *logfile)
{
	log_fd = fopen(logfile, "a");
	if (log_fd == NULL) {
		log_print(LOG_ERROR, "log_open('%s'): %s", logfile);
		return 0;
	}

	if (atexit(log_close) != 0) {
		log_print(LOG_ERROR, "log_open(): atexit()");
		return 0;
	}

	log_print(LOG_EVERYTIME, "==========================");
	return 1;
}
